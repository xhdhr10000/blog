<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Mail\WelcomeAgain;
use App\Http\Requests\RegistrationRequest;

class RegistrationController extends Controller
{
	public function create() {
		return view('registration.create');
	}

	public function store(RegistrationRequest $request) {
		$user = User::create([
			'name' => request('name'),
			'email' => request('email'),
			'password' => bcrypt(request('password'))
			]);

		auth()->login($user);

		// \Mail::to($user)->send(new WelcomeAgain($user));

		session()->flash('message', 'Registration success!');

		return redirect()->home();
	}
}
