@component('mail::message')
Thanks for registering. :)

- First
- Second
- Third

@component('mail::button', ['url' => 'http://localhost:8000'])
Go to homepage
@endcomponent

@component('mail::panel')
Have a nice day!
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
