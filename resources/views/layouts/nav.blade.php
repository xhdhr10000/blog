<div class="blog-masthead">
	<div class="container">
		<nav class="nav blog-nav">
			<a class="nav-link 
				@if ($active == '/')
					active
				@endif
			" href="/">Home</a>

			<a class="nav-link" href="#">New features</a>
			<a class="nav-link" href="#">Press</a>
			<a class="nav-link" href="#">New hires</a>
			<a class="nav-link" href="#">About</a>

			@if (Auth::check())
				<a class="nav-link ml-auto" href="#">{{ Auth::user()->name }}</a>
			@else
				<a class="nav-link ml-auto
					@if ($active == 'login')
						active
					@endif
				" href="/login">Login</a>

				<a class="nav-link
					@if ($active == 'register')
						active
					@endif
				" href="/register">Register</a>
			@endif
		</nav>
	</div>
</div>